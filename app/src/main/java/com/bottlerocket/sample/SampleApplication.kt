package com.bottlerocket.sample

import android.app.Application
import android.content.Context

class SampleApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        context = applicationContext;
    }

    companion object {
        private lateinit var context: Context

        fun getApplicationContext(): Context = context;
    }
}