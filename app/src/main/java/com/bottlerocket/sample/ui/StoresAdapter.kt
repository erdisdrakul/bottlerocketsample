package com.bottlerocket.sample.ui

import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bottlerocket.sample.R
import com.bottlerocket.sample.models.Store
import com.squareup.picasso.Picasso

class StoresAdapter(context: Context) : RecyclerView.Adapter<StoresAdapter.ViewHolder>() {
    private var stores: MutableList<Store>  = ArrayList()
    private var context: Context = context

    fun setStores(stores : MutableList<Store>){
        this.stores = stores
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = stores.get(position)
        holder.bind(item, context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_list_store, parent, false))
    }

    override fun getItemCount(): Int {
        return stores.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val storeLogoURL = view.findViewById(R.id.ivImageStore) as ImageView
        private val name = view.findViewById(R.id.tvNameStore) as TextView
        private val address = view.findViewById(R.id.tvAddressStore) as TextView
        private val phone = view.findViewById(R.id.tvPhoneStore) as TextView

        fun bind(store: Store, context: Context){
            storeLogoURL.loadUrl(store.storeLogoURL)
            name.text = store.name
            address.text = store.address
            phone.text = store.phone
            itemView.setOnClickListener {
                val intent = Intent(itemView.context, DetailStoreActivity::class.java)
                intent.putExtra("id", store.id)
                intent.putExtra("name", store.name)
                intent.putExtra("address", store.address)
                intent.putExtra("city", store.city)
                intent.putExtra("zipcode", store.zipcode)
                intent.putExtra("storeLogoURL", store.storeLogoURL)
                intent.putExtra("phone", store.phone)
                intent.putExtra("state", store.state)
                intent.putExtra("latitude", store.latitude)
                intent.putExtra("longitude", store.longitude)
                itemView.context.startActivity(intent)
            }
        }

        private fun ImageView.loadUrl(url: String) {
            Picasso.with(context).load(url).into(this)
        }

    }
}