package com.bottlerocket.sample.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.bottlerocket.sample.R
import com.bottlerocket.sample.models.Store
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_store.*

class DetailStoreActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_store)

        val store = Store(
            intent.getStringExtra("id"),
            intent.getStringExtra("name"),
            intent.getStringExtra("address"),
            intent.getStringExtra("city"),
            intent.getStringExtra("zipcode"),
            intent.getStringExtra("storeLogoURL"),
            intent.getStringExtra("phone"),
            intent.getStringExtra("state"),
            intent.getStringExtra("latitude"),
            intent.getStringExtra("longitude")
        )

        supportActionBar!!.title = store.name

        ivImageStore.loadUrl(store.storeLogoURL)
        tvNameStore.text = store.name
        tvAddressStore.text = "${store.address}, ${store.city}, ${store.state}, ${store.zipcode}"
        tvPhoneStore.text = store.phone
        tvCoordsStore.text = "${store.latitude},${store.longitude}"
    }

    private fun ImageView.loadUrl(url: String) {
        Picasso.with(context).load(url).into(this)
    }
}
