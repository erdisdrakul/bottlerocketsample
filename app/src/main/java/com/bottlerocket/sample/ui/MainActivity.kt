package com.bottlerocket.sample.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.bottlerocket.sample.R
import com.bottlerocket.sample.models.Store
import com.bottlerocket.sample.viewmodels.StoreList
import com.bottlerocket.sample.viewmodels.StoresViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    /**
     * ViewModel
     */
    private lateinit var model: StoresViewModel

    /**
     *
     */
    private lateinit var recyclerView: RecyclerView

    /**
     * StoresAdapter
     */
    private val adapter = StoresAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        supportActionBar!!.setTitle(R.string.stores)

        setupRecyclerView()

        model = ViewModelProviders.of(this)[StoresViewModel::class.java]
        model.getStores().observe(this, Observer<StoreList>{ storeList ->
            storeListChangedHandler(storeList)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item!!.itemId) {
            R.id.action_refresh -> {
                reloadStore()
                true
            }
            R.id.action_settings -> {
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Initialize recycler view
     */
    private fun setupRecyclerView(){
        recyclerView = findViewById(R.id.rvStoreList)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        adapter.setStores(emptyList<Store>().toMutableList())
        recyclerView.adapter = adapter
    }

    /**
     * This method is called when a change in the store listing has happened.
     * Display the stores or an error message.
     */
    private fun storeListChangedHandler(storeList: StoreList) {
        if (storeList.getError() == null) {
            adapter.setStores(storeList.getStores().toMutableList())
            adapter.notifyDataSetChanged()
        } else {
            showErrorMessage(getString(R.string.server_problems), getString(R.string.server_problems_details))
        }
    }

    /**
     * Call this method to display an error message.
     *
     * @param title Title
     * @param message Message details
     */
    private fun showErrorMessage(title: String, message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(title)
        builder.setMessage(message)

        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
            reloadStore()
        }

        builder.setNegativeButton(android.R.string.cancel) { dialog, which ->

        }

        builder.show()
    }

    /**
     * Call this method to recharge stores.
     */
    private fun reloadStore() {
        model.reload()
    }

}
