package com.bottlerocket.sample.api

/**
 *
 */
data class StoreResponse(
    var storeID: String,
    var name: String,
    var address: String,
    var city: String,
    var state: String,
    var zipcode: String,
    var phone: String,
    var latitude: String,

    var longitude: String,
    var storeLogoURL: String
)

/**
 *
 */
data class StoreListResponse(
    var stores: List<StoreResponse>
)
