package com.bottlerocket.sample.api

import retrofit2.Call
import retrofit2.http.GET

/**
 * Store API interface
 */
interface StoresService {
    @GET("stores.json")
    fun listStores(): Call<StoreListResponse>
}