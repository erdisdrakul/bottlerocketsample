package com.bottlerocket.sample.api

import com.bottlerocket.sample.models.Store
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class StoresApiClient {
    private var retrofit: Retrofit = Retrofit.Builder()
        .baseUrl("http://sandbox.bottlerocketapps.com/BR_Android_CodingExam_2015_Server/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    /**
     * Call this method to get the list of all stores.
     *
     * @return Store list
     */
    fun fetch(): List<Store> {
        val response = retrofit.create(StoresService::class.java).listStores().execute()
        val storesListResponse = response.body() as StoreListResponse

        val stores = mutableListOf<Store>()
        for (storeResponse in storesListResponse.stores) {
            stores.add(mapStoreResponseToStoreModel(storeResponse))
        }
        return stores
    }

    /**
     * Call this method to map an instance of StoreResponse object to Store model.
     *
     * @return Store model instance.
     */
    private fun mapStoreResponseToStoreModel(storeResponse: StoreResponse): Store {
        return Store(
            storeResponse.storeID,
            storeResponse.name,
            storeResponse.address,
            storeResponse.city,
            storeResponse.zipcode,
            storeResponse.storeLogoURL,
            storeResponse.phone,
            storeResponse.state,
            storeResponse.latitude,
            storeResponse.longitude
        )
    }
}