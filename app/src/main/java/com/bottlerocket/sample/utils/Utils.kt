package com.bottlerocket.sample.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.bottlerocket.sample.SampleApplication

object Utils {
    /**
     * Check availability of the internet connection.
     *
     * @return true if connection, false otherwise
     */
    fun isNetworkAvailable(): Boolean {
        val context = SampleApplication.getApplicationContext()
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }
}