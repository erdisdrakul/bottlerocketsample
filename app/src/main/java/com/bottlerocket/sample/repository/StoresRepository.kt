package com.bottlerocket.sample.repository

import com.bottlerocket.sample.SampleApplication
import com.bottlerocket.sample.models.Store
import org.jetbrains.anko.db.BLOB
import org.jetbrains.anko.db.TEXT
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import java.time.LocalDate

/**
 * This class will be used to chache stores list
 */
class StoresRepository {
    private var lastSave = 0L

    /**
     * If this method returns true it means that the data is expired
     * and it is necessary to update them
     */
    fun isCacheExpired(): Boolean {
        return System.currentTimeMillis() - lastSave > 300000;
    }

    /**
     *
     */
    fun saveAll(stores: List<Store>) {
        SampleApplication.getApplicationContext().database.use {
            for (store in stores) {
                insert("Stores",
                    "id" to store.id,
                    "name" to store.name,
                    "address" to store.address,
                    "city" to store.city,
                    "state" to store.state,
                    "zip" to store.zipcode,
                    "phone" to store.phone,
                    "latitude" to store.latitude,
                    "longitude" to store.longitude,
                    "logoUrl" to store.storeLogoURL
                )
            }
        }
        lastSave = System.currentTimeMillis();
    }

    /**
     * Call this method to get store listings.
     */
    fun fetchAll(): List<Store> {
        val stores = mutableListOf<Store>()
        SampleApplication.getApplicationContext().database.use {
            select("Stores",
                "id", "name", "address", "city", "zip", "logoUrl", "phone", "state", "latitude", "longitude").exec {
                if (count > 0) {
                    moveToFirst()
                    do {
                        val store = Store(
                            getString(0),
                            getString(1),
                            getString(2),
                            getString(3),
                            getString(4),
                            getString(5),
                            getString(6),
                            getString(7),
                            getString(8),
                            getString(8)
                        )
                        stores.add(store)
                    } while (moveToNext())
                }
            }
        }
        return stores
    }
}