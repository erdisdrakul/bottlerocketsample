package com.bottlerocket.sample.repository

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

class DatabaseHelper private constructor(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "MyDatabase", null, 1) {
    init {
        instance = this
    }

    companion object {
        private var instance: DatabaseHelper? = null

        @Synchronized
        fun getInstance(ctx: Context) = instance ?: DatabaseHelper(ctx.applicationContext)
    }

    override fun onCreate(db: SQLiteDatabase) {
        // Create stores table
        db.createTable("Stores", true,
            "id" to TEXT + PRIMARY_KEY + UNIQUE,
            "name" to TEXT,
            "address" to TEXT,
            "city" to TEXT,
            "state" to TEXT,
            "zip" to TEXT,
            "phone" to TEXT,
            "latitude" to TEXT,
            "longitude" to TEXT,
            "logoUrl" to TEXT)

        db.createTable("Cache", true,
            "id" to INTEGER + PRIMARY_KEY + UNIQUE,
            "name" to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable("Stores", true)
    }
}

val Context.database: DatabaseHelper get() = DatabaseHelper.getInstance(this)