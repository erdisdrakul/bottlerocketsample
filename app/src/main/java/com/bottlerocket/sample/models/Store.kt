package com.bottlerocket.sample.models

/**
 * Store model.
 */
data class Store(
    var id:String,
    var name:String,
    var address:String,
    var city:String,
    var zipcode:String,
    var storeLogoURL:String,
    var phone:String,
    var state:String,
    var latitude:String,
    var longitude:String
)