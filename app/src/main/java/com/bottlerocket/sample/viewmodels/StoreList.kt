package com.bottlerocket.sample.viewmodels

import com.bottlerocket.sample.models.Store

/**
 * Wrapper class that contains list stores and error code
 */
class StoreList(values: List<Store>, error: Error?) {
    private var stores: MutableList<Store> = values.toMutableList()
    private var error = error

    /**
     * Returns store list.
     */
    fun getStores(): List<Store> {
        return stores;
    }

    /**
     * Return error.
     */
    fun getError(): Error? {
        return error;
    }
}