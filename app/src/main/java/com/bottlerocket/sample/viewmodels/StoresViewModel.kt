package com.bottlerocket.sample.viewmodels


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bottlerocket.sample.api.StoresApiClient
import com.bottlerocket.sample.models.Store
import com.bottlerocket.sample.repository.StoresRepository
import com.bottlerocket.sample.utils.Utils
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception

class StoresViewModel: ViewModel() {
    /**
     * Cache
     */
    private var repository = StoresRepository()

    private val storeList: MutableLiveData<StoreList> by lazy {
        MutableLiveData<StoreList>().also {
            loadStoresInBackground()
        }
    }

    /**
     * Call this method to refresh the store list.
     */
    fun reload() {
        storeList.value = StoreList(emptyList(), null)
        loadStoresInBackground()
    }

    /**
     * LiveData method to listen to changes in the store list.
     */
    fun getStores(): LiveData<StoreList> {
        return storeList;
    }

    /**
     * Load stores in background
     */
    private fun loadStoresInBackground() {
        if (Utils.isNetworkAvailable()) {
            doAsync {
                try {
                    var stores: List<Store>?
                    if (repository.isCacheExpired()) {
                        stores = StoresApiClient().fetch()
                        repository.saveAll(stores)
                    } else {
                        stores = repository.fetchAll()
                    }

                    uiThread {
                        storeList.value = StoreList(stores, null)
                    }
                } catch (e: Exception) {
                    uiThread {
                        storeList.value = StoreList(emptyList(), Error("Server error"))
                    }
                }

            }
        } else {
            storeList.value = StoreList(emptyList(), Error("Internet connection error"))
        }
    }
}